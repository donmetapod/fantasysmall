﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

	//[SerializeField] private Vector3 direction;
	[SerializeField] private Transform target;
	[SerializeField] private float translationSpeed;
	[SerializeField] private float rotationSpeed;
	
	void Update ()
	{
		transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * translationSpeed);
		transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, target.eulerAngles, Time.deltaTime * rotationSpeed);
	}
}
