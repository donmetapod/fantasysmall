﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : MonoBehaviour
{
	[SerializeField]private Material daylightSkybox;
	[SerializeField]private Material nightSkybox;

	private void Start()
	{
		RenderSettings.skybox = daylightSkybox;
	}
	
	public void SetNighSkybox()
	{
		RenderSettings.skybox = nightSkybox;
	}
}
